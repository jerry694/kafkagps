package com.dailycodebuffer.cabbookclient.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class LocationService {
    @KafkaListener(topics = "cab-book-driver",groupId = "user-group")
    public void getLocation(String location) {
        System.out.println(location);
    }
}
