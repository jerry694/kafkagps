package com.dailycodebuffer.cabbookclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CabBookClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(CabBookClientApplication.class, args);
	}

}
